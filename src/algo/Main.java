package algo;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Classe permettant la mesure des tps d'executions
 * 
 * @author Kevin COQUART
 * 
 */
public class Main {
	/**
	 * Compresse, decompresse le fichier pass� en argument et ajoute les tps au
	 * fichier csv
	 * 
	 * @param args
	 *            args1 nom du fichier
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 1)
			System.out.println("Erreur 1 arguments\n\t1) Nom du fichier\n");
		else {
			FileWriter fw = new FileWriter("result.csv", true);
			long startC = System.currentTimeMillis();
			int nbChar = Compression.compresse(args);
			long stopC = System.currentTimeMillis();

			args[0] = args[0].replace(".txt", ".cmpr");
			long startD = System.currentTimeMillis();
			Decompression.main(args);
			long stopD = System.currentTimeMillis();

			fw.write(args[0] + "," + nbChar + "," + (stopC - startC) + ","
					+ (stopD - startD) + "\n");
			fw.close();
		}
	}
}
