package algo;

/**
 * Classe qui represente un arbre
 * 
 * @author Kevin COQUART
 * 
 */
class Arbre {
	int poids, lettre;
	Arbre pere, filsG, filsD;
	Arbre prec, suiv;
	boolean estFilsD;

	/**
	 * Creer une feuille
	 * 
	 * @param d
	 *            vrai si l'arbre est un fils droit
	 * @param w
	 *            le poids
	 * @param l
	 *            la lettre
	 * @param p
	 *            le pere
	 * @param prec
	 *            l'arbre precedant dans la liste
	 * @param suiv
	 *            l'arbre suivant dans la liste
	 */
	Arbre(boolean d, int w, int l, Arbre p, Arbre prec, Arbre suiv) {
		estFilsD = d;
		poids = w;
		lettre = l;
		pere = p;
		this.prec = prec;
		this.suiv = suiv;
		filsG = null;
		filsD = null;
		if (suiv != null)
			suiv.prec = this;
		if (prec != null)
			prec.suiv = this;
		if (pere != null) {
			if (estFilsD)
				pere.filsD = this;
			else
				pere.filsG = this;
		}
	}

	/**
	 * l'arbre est-il une feuille
	 * 
	 * @return vrai si c'est une feuille
	 */
	boolean estFeuille() {
		return ((filsD == null) && (filsG == null));
	}

	/**
	 * Renvoie en string le code de la racine au noeud courant
	 * 
	 * @return la chaine de caractere correspondant au code du noeud courant
	 */
	String chemin() {
		if (pere != null)
			if (estFilsD)
				return pere.chemin() + "1";
			else
				return pere.chemin() + "0";
		return "";
	}

	/**
	 * Supprimer le noeud de la liste chainee, on suppose prec et suiv non null
	 */
	void supprimer() {
		prec.suiv = suiv;
		suiv.prec = prec;
	}

	/**
	 * Insere le noeud apres l'arbre a dans la liste chainee. a.suivant != null
	 * 
	 * @param a
	 *            un arbre
	 */
	void insererApres(Arbre a) {
		suiv = a.suiv;
		prec = a;
		suiv.prec = this;
		prec.suiv = this;
	}

	/**
	 * Renvoie le plus grand element de meme poids de la liste
	 * 
	 * @return un arbre plus grand que le noeud courant et de meme poids
	 */
	Arbre plusGrandMemePoids() {
		if ((suiv == null) || (suiv.poids > poids))
			return this;
		else
			return suiv.plusGrandMemePoids();
	}

	/**
	 * Mets a  jour les poids en partant du noeud courant
	 */
	void traitement() {
		// On recupere l'arbre de meme poids le plus loin dans la liste
		Arbre a = plusGrandMemePoids();
		if (a != pere)
			echanger(a);
		poids++;
		if (pere != null)
			pere.traitement();
	}

	/**
	 * On echange le noeud courant avec a
	 * 
	 * @param a
	 */
	void echanger(Arbre a) {
		if (a == this)
			return; // pas d'echange

		Arbre aux = pere;
		pere = a.pere;
		a.pere = aux;

		boolean symb = estFilsD;
		estFilsD = a.estFilsD;
		a.estFilsD = symb;

		aux = prec;
		supprimer();
		insererApres(a);
		a.supprimer();
		a.insererApres(aux);

		if (estFilsD)
			pere.filsD = this;
		else
			pere.filsG = this;

		if (a.estFilsD)
			a.pere.filsD = a;
		else
			a.pere.filsG = a;
	}

	/**
	 * Renvoie la feuille contenant s
	 * 
	 * @param s
	 *            le caractere
	 * @return la feuille avec s
	 */
	/*
	 * public Arbre getFeuille(int s) { if (estFeuille()) if (lettre == s)
	 * return this; else return null;
	 * 
	 * Arbre fg = filsG.getFeuille(s), fd = filsD.getFeuille(s); if (fg != null)
	 * return fg; else if (fd != null) return fd; else return null; }
	 */
}