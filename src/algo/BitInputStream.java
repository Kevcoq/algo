package algo;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Classe permettant la lecture dans un flux bit � bit
 * 
 * @author project Excilys
 */
public class BitInputStream extends FilterInputStream {
	private int cpt_bit, bit_restant;

	/**
	 * Constructeur
	 * 
	 * @param in
	 *            le flux d'entree
	 */
	protected BitInputStream(InputStream in) {
		super(in);
	}

	/**
	 * Lis un bit dans le flux
	 * 
	 * @return un entier correspondant au bit, 0 ou 1
	 * @throws IOException
	 */
	public int readBit() throws IOException {
		if (cpt_bit == 0) {
			bit_restant = super.read();
			cpt_bit = 8;

			if (bit_restant < 0) {
				cpt_bit = 0;
				throw new IOException();
			}
		}
		return (bit_restant & mask(--cpt_bit)) >> cpt_bit;
	}

	private static byte mask(int bitNumber) {
		if (bitNumber < 0 || bitNumber > 8) {
			throw new IllegalArgumentException();
		}
		return (byte) Math.pow(2, bitNumber);
	}
}
