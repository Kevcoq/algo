package algo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Classe de Compression
 * 
 * @author Kevin COQUART
 * 
 */
public class Compression {
	/**
	 * Compresse le texte pass� en argument
	 * 
	 * @param args
	 *            le nom du fichier
	 * @return 
	 */
	public static int compresse(String[] args) {
		try {
			/* On ouvre les differents flux */
			FileInputStream input = new FileInputStream(args[0]);
			FileOutputStream output = new FileOutputStream(args[0].replace(
					".txt", ".cmpr"));
			BitOutputStream bos = new BitOutputStream(output);

			/* Petite phrase d'intro */
			System.out.println("Compression de " + args[0]);

			/* On creer un objet code */
			Code h = new Code();

			/* Puis on itere par ajout de chaque caractere rencontre */
			int c = input.read();
			int nbChar = 1;
			while (c != -1) {
				/* On obtient la lettre sous forme de string binaire */
				String code = h.ajouterLettre(c);
				/* On ecrit chaque caractere de la string sous forme de bit */
				for (int i = 0; i < code.length(); i++)
					if (code.charAt(i) == '0')
						bos.writeBit(0);
					else
						bos.writeBit(1);
				c = input.read();
				nbChar++;
			}

			/* Petite phrase de fin */
			System.out.println("Fichier compress� !");

			/* On referme les flux */
			bos.flush();
			bos.close();
			input.close();

			return nbChar;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}


	/**
	 * Le main
	 * @param args
	 */
	public static void main(String[] args) {
		compresse(args);
	}
}