package algo;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Classe permettant de mesurer les tps d'�xecution, choix fichier graphique
 * 
 * @author Kevin COQUART
 * 
 */
public class Fenetre {
	/**
	 * Permet de choisir le fichier texte d'entr�e puis lance le main globale
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		JFileChooser choix = new JFileChooser(new File("."));
		choix.setFileFilter(new FileNameExtensionFilter("Fichier texte", "txt"));

		if (choix.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			String[] arg = new String[1];
			arg[0] = choix.getSelectedFile().getName();
			Main.main(arg);
		}
	}

}
