package algo;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Classe de decompression
 * 
 * @author Kevin COQUART
 * 
 */
public class Decompression {
	/**
	 * Decompressse le fichier passé en argument
	 * 
	 * @param args
	 *            le nom du fichier
	 */
	public static void decompresse(String[] args) {
		try {
			/* On ouvre les differents flux */
			FileInputStream input = new FileInputStream(args[0]);
			BitInputStream bis = new BitInputStream(input);
			PrintWriter pw = new PrintWriter(new FileWriter(args[0].replace(
					".cmpr", ".decmpr")));

			/* Petite phrase d'intro */
			System.out.println("Décompression de " + args[0]);

			/* On creer un objet code */
			Code h = new Code();

			try {
				/* Puis on itere par ajout de chaque caractere rencontre */
				while (true) {
					/* On obtient la lettre */
					char s = h.decode(bis);
					/* On ecrit le caractere */
					pw.print(s);
				}
			} catch (IOException e) {
			}

			/* Petite phrase de fin */
			System.out.println("Fichier decompressé !");

			/* On referme les flux */
			pw.close();
			input.close();
		} catch (IOException e) {
		}
	}
	
	
	
	/**
	 * Le main
	 * @param args
	 */
	public static void main(String[] args) {
		decompresse(args);
	}
}
