package algo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe qui regroupe la structure des algos et leur fonction
 * 
 * @author Kevin COQUART
 * 
 */
public class Code {
	Arbre racine;
	Arbre liste;
	Map<Integer, Arbre> map;

	final static int tailleAlphabet = 8;
	final static int valeurMaxLettre = (int) Math.pow(2, tailleAlphabet);

	/**
	 * Construit une feuille vide
	 */
	Code() {
		racine = liste = new Arbre(false, // arbitraire
				0, // poids
				-1, // pas de lettre
				null, null, null);
		map = new HashMap<>();
	}

	/**
	 * Retourne le binaire associ� � la lettreF
	 * 
	 * @param lettre
	 *            le caractere a traduire
	 * @return le binaire du caractere
	 */
	static String baseDeux(int lettre) {
		String s = "";
		for (int i = 0; i < tailleAlphabet; ++i) {
			s = (lettre % 2) + s;
			lettre /= 2;
		}
		return s;
	}

	/**
	 * Lis le 1er caractere dans le flux binaire
	 * 
	 * @param bis
	 *            un flux binaire
	 * @return le 1 caractere du flux
	 * @throws IOException
	 */
	static int lireLettreBinaire(BitInputStream bis) throws IOException {
		int l = 0;
		for (int i = 0; i < tailleAlphabet; i++)
			if (bis.readBit() == 1)
				l += Math.pow(2, tailleAlphabet - 1 - i);
		return l;
	}

	/**
	 * Renvoie le code correspondant � la lettre
	 * 
	 * @param lettre
	 *            le caractere a compress�
	 * @return le code qui correspond dans l'arbre
	 */
	String ajouterLettre(int lettre) {
		String motDeCode = "";
		// Arbre a = racine.getFeuille(lettre);
		Arbre a = map.get(lettre);
		if (a == null) {
			a = liste;
			ajouterNouvelleLettre(lettre);
			motDeCode = baseDeux(lettre);
		}
		motDeCode = a.chemin() + motDeCode;
		a.traitement();
		return motDeCode;
	}

	/**
	 * Ajoute une nouvelle lettre � l'arbre
	 * 
	 * @param lettre
	 *            le caractere a ajouter
	 */
	void ajouterNouvelleLettre(int lettre) {
		Arbre aux = liste;
		Arbre tmp = new Arbre(true, 1, lettre, liste, null, liste);
		liste = new Arbre(false, 0, -1, aux, null, tmp);
		map.put(lettre, tmp);
	}

	/**
	 * Decode le 1er caractere du flux binaire
	 * 
	 * @param bis
	 *            le flux binaire
	 * @return le 1er caractere du flux
	 * @throws IOException
	 */
	char decode(BitInputStream bis) throws IOException {
		Arbre a = racine;
		int lettre;
		while (true) {
			if (a.estFeuille()) {
				if (a.lettre == -1) {
					a = liste;
					lettre = lireLettreBinaire(bis);
					ajouterNouvelleLettre(lettre);
				} else {
					lettre = a.lettre;
				}
				a.traitement();
				return (char) lettre;
			}

			int b = bis.readBit();
			if (b == 0) {
				a = a.filsG;
			} else {
				a = a.filsD;
			}
		}
	}
}
